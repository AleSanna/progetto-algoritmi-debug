#include <iostream>
#include <fstream>
#include <string>
#include "VeriLog.h"

//Tutti gli switch dentro while e possibilità di tornare al menù principale

using namespace std;

int main(int argc ,char *argv[]) {

    VeriLog Circuito;
    char inputFromUser = '"', inputFromUser2 = '"';
    string fileName;
    vector<string> dataPrinter;
    ofstream outputFile;
    ifstream inputFile;
    string msg_choice = "\nThe functionalities are:\n(a)Simulation\n(b)Analisis\n"
                        "(q)Quit program\nPlease choose one of the previous:\t";

    string msg_errorChoice1 = "\nNot valid choice. Please insert one character from the list below:\n(a)Simulation\n(b)Analisis\n"
                              "(q)Quit program";

    Circuito.readAndCreate(argv[1]);

    cout << "\n\t...Sistema di simulazione di reti logiche..." << endl;

    do {
        cout << msg_choice;
        cin >> inputFromUser;

        switch (inputFromUser)
        {
            case 'a':
            case 'A':

                cout << "\n\t...Starting Simulation...\n" << endl << "Please insert the inputFile.txt name: ";
                cin >> fileName;

                if (fileName.find(".txt") == string::npos)                                         //Corregge il nome se non presente il .txt
                    fileName += ".txt";

                inputFile.open(fileName);

                if (!inputFile.is_open()) {
                    cerr << "Error: output File could not be open or does not exist." << endl;
                }
                else{
                    dataPrinter = Circuito.getData_Simulation(inputFile);

                    cout << "\nSimulation executed. Would you like to:\n(a) Print data on desktop" << endl;
                    cout << "(b) Insert data into a file" << endl << "Choice: ";
                    cin >> inputFromUser2;

                    switch (inputFromUser2) {
                        case 'a':
                        case 'A':

                            for (int i = 0; i < dataPrinter.size(); ++i) {
                                cout << dataPrinter[i] << endl;
                            }
                            break;
                        case 'b':
                        case 'B':

                            cout << endl << "Insert file name: ";
                            cin >> fileName;

                            if (fileName.find(".txt") ==
                                string::npos)                                         //Corregge il nome se non presente il .txt
                                fileName += ".txt";

                            outputFile.open(fileName);

                            if (!outputFile.is_open()) {
                                cerr << "Error: output File could not be open or does not exist." << endl;
                                return -2;
                            }
                            else{
                                for (int i = 0; i < dataPrinter.size(); ++i) {
                                    outputFile << dataPrinter[i] << endl;

                                }
                                cout << "\n" << fileName << " succesfully created.\n";
                            }

                            outputFile.close();
                            break;

                        default:
                            cout << "Not valid choice. Please insert one character from the list below: '\n'(a) Print data on desktop" << endl;
                            cout << "(b) Insert data into a file" << endl << "Choice: ";
                            cin >> inputFromUser2;
                    }

                }
                break;

            case 'b':
            case 'B':

                cout << "\n\t...Starting Analisis...\n" << endl;
                cout << "The functionalities are:\n(a)Minimun Path\n(b)Maximun Path"
                        "\n(c)Logic Cones\n(d)Power Analisis\n(r)Return to Main Page\n(q)Quit program\nPlease choose one of the previous: ";
                cin >> inputFromUser2;

                switch (inputFromUser2) {
                    case 'a':
                    case 'A':



                        break;
                    case 'b':
                    case 'B':

                        break;

                    case 'c':
                    case 'C':

                        break;

                    case 'd':
                    case 'D':

                        break;

                    case 'r':
                    case 'R':
                        cout << "\n\t...Returning back to main page..." << endl;
                        break;


                    case 'q':
                    case 'Q':
                        inputFromUser = 'q';
                        break;

                    default:
                        cout << "\nNot valid choice. Please insert one character from the list below:\n(a)Minimun Path\n(b)Maximun Path"
                                "\n(c)Logic Cones\n(d)Power Analisis\n(r)Return to Main Page\n(q)Quit program" << endl << "Choice: ";
                        cin >> inputFromUser2;
                }
                break;

            case 'q':
            case 'Q':
                cout << "\n\t...Closing programm..." << endl;
                //exit(EXIT_SUCCESS);
                break;

            default:
                cout << msg_errorChoice1 << endl << "\nChoice: ";
                cin >> inputFromUser;
        }
    }while(inputFromUser != 'q' && inputFromUser2 != 'q');

    return 0;
}