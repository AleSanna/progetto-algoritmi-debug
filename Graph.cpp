//
// Created by stree on 22/06/2019.
//

#include "Graph.h"
#include <string>
#include <sstream>

using namespace std;

Graph::Graph(){
}

Graph::~Graph(){
}

void Graph::insertNodeLoop(const string &name, const bool &is_input, const bool &is_loop) // usata per i loop
{
    node to_add;
    to_add.name = name;
    to_add.is_input = is_input;
    to_add.is_next = is_loop;
    to_add.is_loop= is_loop;
    to_add.inputValue1 = false;
    to_add.inputValue2 = false;
    to_add.outputValue = false;
    to_add.nextnode = -1;
    circuito.push_back(to_add);
}

void Graph::insertNode(const string &name, const bool &is_input) // usata per gli input
{
    node to_add;
    to_add.name = name;
    to_add.is_input = is_input;
    to_add.is_next = is_input;
    to_add.is_loop = false;
    to_add.inputValue1= false;
    to_add.inputValue2= false;
    to_add.outputValue= false;
    to_add.nextnode = -1;
    circuito.push_back(to_add) ;
}

void Graph::insertNode(const string &name, const bool &is_input, const string &logicInfo) //usata per le porte logiche
{
    node to_add;
    to_add.logicInfo=logicInfo;
    to_add.name = name;
    to_add.is_input = is_input;
    to_add.is_input= false;
    to_add.is_loop= false;
    to_add.is_next = false;
    to_add.inputValue1= false;
    to_add.inputValue2= false;
    to_add.outputValue= false;
    to_add.nextnode = -1;
    circuito.push_back(to_add);
}

void Graph::insertLink(const string &start, const string &end)
{
    bool flag = false;
    int i = 0;
    int position_start =-1 , position_end=-1;
    while ((!flag) && (i < circuito.size()))                                        //si potrebbe fare un while a doppio flag
    {
        if (circuito[i].name == start)
        {
            flag = true;
            position_start = i;
        }
        i++;
    }
    flag = false;
    i = 0;
    while ((!flag) && (i < circuito.size()))
    {
        if (circuito[i].name == end)
        {
            flag = true;
            position_end = i;
        }
        i++;
    }
    if ((position_start != -1) && (position_end != -1)){
        circuito[position_start].nextnode = position_end;
    }

}

void Graph::MaxAndMinPath()
{
    Min_Path = INT_MAX;
    Max_Path = -1;
    vector <string> temp;
    string search;
    int p;
    int counter;

    for (counter = 0; counter < circuito.size(); counter++)
    {
        if (circuito[counter].is_input)
        {
            int z, i = 0;

            search = circuito[counter].name;
            p = counter;
            z = 0;
            temp.clear();

            while ((circuito[p].nextnode != -1) && (i < circuito.size()))
            {
                p = circuito[p].nextnode;
                temp.push_back(circuito[p].logicInfo);
                z++;
            }
            if(z< Min_Path)
            {
                Min_Path = z;
                Min_Search = search;
                Min_Vec = temp;
            }
            if(z> Max_Path)
            {
                Max_Path = z;
                Max_Search = search;
                Max_Vec = temp;
            }
        }
    }
}

string Graph::PrintPathMax()
{
    int i;
    string stamp;
    ostringstream s;
    s << "il percorso piu' lungo: " << Max_Search << " con " << Max_Path << " nodi: ";
    for (i = 0; i < Max_Vec.size(); i++)
    {
        s << Max_Vec[i] << " ";
    }
    s << endl;
    stamp = s.str();
    return stamp;
}

string Graph::PrintPathMin()
{
    int i;
    string stamp;
    ostringstream s;
    s << "il percorso piu' corto: " << Min_Search << " con " << Min_Path << " nodi: ";
    for (i = 0; i < Min_Vec.size(); i++)
    {
        s << Min_Vec[i] << " ";
    }
    s << endl;
    stamp = s.str();
    return stamp;
}

string Graph::checkPresence(string to_search) {

    vector<node>::iterator it = circuito.begin();
    ostringstream number;
    string temp;
    bool flag;
    int index=1;
    string ret = to_search;


    do{
        flag = false;

        //for (int i = 0; i < circuito.size(); ++i) {
        for (; it < circuito.end(); it++) {
            number << index;
            if (it->name == to_search) {
                flag = true;
                to_search= ret+"$$"+number.str();     //Decidere se mettere spazio o meno
                index++;
            }
            number.str("");                             //clear forzata
        }

    }while(flag);

    return to_search;
}

bool Graph::calculator( vector<power> dataPower) {
    bool result;
    int i,j,temp;
    bool flag = false, exit;
    vector<node> circuitoTemp = circuito;

    while (!flag)
    {
        for (i = 0; i < circuitoTemp.size() && !flag; i++)
        {
            if (circuitoTemp[i].is_next)
            {
                temp = circuitoTemp[i].nextnode; // il flip flop usa solo inputValue1
                if (circuitoTemp[temp].logicInfo == "NOT" || circuitoTemp[temp].logicInfo == "FF"){

                    if(circuitoTemp[temp].logicInfo =="FF"){
                        circuitoTemp[temp].outputValue = circuitoTemp[temp].inputValue1;
                    }
                    else{
                        circuitoTemp[temp].outputValue = !circuitoTemp[i].outputValue; // operazione di not non sicura;
                    }

                    circuitoTemp[temp].inputValue1 = circuitoTemp[i].outputValue;
                    circuitoTemp[i].is_next = false;
                    circuitoTemp[temp].is_next = true;
                    //powerCalculator(temp, dataPower);  // qui è dove si calcola anche la potenza
                    if (circuitoTemp[temp].nextnode == -1){
                        result = circuitoTemp[temp].outputValue;
                        flag = true;
                    }
                }
                else            //Per le restanti porte
                {
                    exit = false;
                    for (j = 0; j < circuitoTemp.size() && !exit; j++) //parte con
                    {
                        if ((circuitoTemp[j].nextnode == circuitoTemp[i].nextnode) && (circuitoTemp[j].is_next) && (circuitoTemp[i].is_next) && (i != j))
                        {
                            exit = true;
                            temp = circuitoTemp[j].nextnode;
                            circuitoTemp[temp].outputValue = logicOperation(circuitoTemp[i].outputValue, circuitoTemp[j].outputValue, circuitoTemp[temp].logicInfo); // qui si deve mettere la diversa operazione in base a logic info
                            circuitoTemp[temp].is_next = true;
                            circuitoTemp[temp].inputValue1= circuitoTemp[i].outputValue;
                            circuitoTemp[temp].inputValue2= circuitoTemp[j].outputValue;
                            circuitoTemp[j].is_next = false;
                            circuitoTemp[i].is_next = false;
                            //powerCalculator(temp,dataPower); // calcolo della potenza
                            if (circuitoTemp[temp].nextnode == -1) // controllo di non essere giunto all'ultimo nodo
                            {
                                result = circuitoTemp[temp].outputValue;
                                flag = true;
                            }

                        }
                    }
                }
            }
        }
    }

    return result;                                                                    //da inserire controllo errore

}

bool Graph::logicOperation(bool op1, bool op2, string logicGate) {                      //da rivedere con switch
    bool result;

    if( logicGate == "AND"){
        result = op1 && op2;
    }
    else{
        if(logicGate == "OR"){
            result = op1 || op2;
        }
        else{
            if(logicGate == "XOR"){
                if(op1 && op2)
                    result = false;
                else
                    result = op1 || op2;
            }
            else{
                if(logicGate == "NAND"){
                    result = !(op1 && op2);
                }
                else{
                    if(logicGate == "NOR"){
                        result = !(op1 || op2);
                    }
                    else{
                        if(logicGate == "XNOR"){
                            result = op1 == op2;                   //Se sono uguali: true  //Se sono diversi: false
                        }
                    }
                }
            }
        }
    }
    return result;
}                                                                    //da inserire controllo errore




void Graph::setter(vector<in> _inputVect) {
    int i,j;
    bool exit;
    for(i=0;i<circuito.size();i++){
        exit= false;                                         // NEW!!! Metodo che riceve il vettore input e assegna i valori al circuito.
        j=0;
        while(!exit && j< _inputVect.size()){
            if(dollarConverter(circuito[i].name) == _inputVect[j].name) {     //verificare se serve il controllo su is_input
                exit= true;
                circuito[i].outputValue = _inputVect[j].value;
                circuito[i].is_next = true;                                     //Lo salva per tutti i cicli di simulazione
            }
            j++;
        }
    }
}// prima di chiamare la funzione di calculate deve essere preceduta da i due setter input e flipflop (questo è per i loop)


void Graph::setter(vector<flip> _flipflop) {
    int i,j;
    bool exit;
    for(i=0;i<circuito.size();i++){
        exit= false;                                         // NEW!!! Metodo che riceve il vettore input e assegna i valori al circuito.
        j=0;
        while(!exit && j< _flipflop.size()){
            if((dollarConverter(circuito[i].name) == _flipflop[j].name) && circuito[j].is_loop) {     //va modificato il controllo per accettare anche A1 ecc. MATTEO{
                exit= true;
                circuito[i].inputValue1 = _flipflop[j].previousValue;
            }
            j++;
        }
    }
}



string Graph::equationParser(string equation,string name,vector<in> _inputVect,vector<genout> GeneralOutput,vector<flip> _flipflop) {
    size_t pos_start, pos_end;
    string substring;
    istringstream getData;
    string first, third, second, replace="SOST";
    string temp,fliptemp;

    if(parenthesisCounter(equation)==logicCounter(equation)-1) {              //Controllo su vettore di LogicGate
        while (equation != temp) {
            pos_start = equation.rfind('(');

            if (pos_start != string::npos) {
                pos_end = equation.find(')', pos_start);
                substring = equation.substr(pos_start + 1, pos_end - pos_start - 1);

                getData.str(substring);
                getData >> first >> second >> third;
                getData.clear();

                if (first == "NOT") {
                    for (int i = 0; i < _inputVect.size(); i++) {
                        if (second == _inputVect[i].name) {
                            second = checkPresence(second);       //Verificare la reference e il counter/index
                            insertNode(second, true);
                        }
                    }
                    for(int j=0 ;j < GeneralOutput.size();j++)
                    { if(second == GeneralOutput[j].name && second != name)
                        {
                            second = equationParser(GeneralOutput[j].equation,GeneralOutput[j].name,_inputVect,GeneralOutput,_flipflop);
                        }

                    }
                    for(int z=0; z<_flipflop.size();z++){
                        if( second == _flipflop[z].name && second != name){
                            fliptemp = equationParser(_flipflop[z].equation,_flipflop[z].name,_inputVect,GeneralOutput,_flipflop);
                            second=checkPresence(second);
                            insertNode(second,false,"FF"); //qui inserisco il flipflop
                            insertLink(fliptemp,second);
                        }

                    }
                    if(second == name)      //sempre per i loop matteo
                    {
                        second=checkPresence(second);
                        insertNodeLoop(second,false,true);
                        loopRequired = true;
                    }
                    temp = checkPresence(replace);
                    insertNode(temp, false, first);
                    insertLink(second,temp);

                    equation.replace(pos_start, pos_end - pos_start + 1, temp);                       //Perché il +1??
                    getData.clear();


                } else {
                    for (int i = 0;i < _inputVect.size(); i++) {                   //Se li trovi entrambi, esci prima DA FARE
                        if(first == _inputVect[i].name){
                            first = checkPresence(first);
                            insertNode(first, true);
                        }

                        if(third == _inputVect[i].name){               //C' ERANO ERRORI NELL' INSERT LINK RISOLTI!!!!
                            third = checkPresence(third);
                            insertNode(third, true);
                        }
                    }
                    for(int j=0 ;j< GeneralOutput.size();j++)
                    {
                        if(first == GeneralOutput[j].name && first != name)
                        {
                            first = equationParser(GeneralOutput[j].equation,GeneralOutput[j].name,_inputVect,GeneralOutput,_flipflop);
                        }
                        if(third== GeneralOutput[j].name && third!= name)  // CONTROLLARE CHE EQUATION RITORNI L' ULTIMO NODO INSERITO
                        {
                            third = equationParser(GeneralOutput[j].equation,GeneralOutput[j].name,_inputVect,GeneralOutput,_flipflop);
                        }
                    }
                    for(int z=0; z<_flipflop.size();z++){
                        if( first == _flipflop[z].name && first != name ){
                            fliptemp = equationParser(_flipflop[z].equation,_flipflop[z].name,_inputVect,GeneralOutput,_flipflop);
                            first= checkPresence(first);
                            insertNode(first,false,"FF"); //qui inserisco il flipflop
                            insertLink(fliptemp,first);
                        }   //ANCHE PER I FLIPFLOP USIAMO LA CHECKPRESENCE O NO?  esempio FF1 = (FF2 and b) or FF2
                        if( third == _flipflop[z].name  && third != name){
                            fliptemp = equationParser(_flipflop[z].equation,_flipflop[z].name,_inputVect,GeneralOutput,_flipflop);
                            third=checkPresence(third);
                            insertNode(third,false,"FF"); //qui inserisco il flipflop
                            insertLink(fliptemp,third);
                        }
                    }
                    if(first== name) // per i loop
                    {
                        first=checkPresence(first);
                        insertNodeLoop(first,false,true);
                        loopRequired= true;
                    }
                    if (third ==name)
                    {
                        third=checkPresence(third);
                        insertNodeLoop(third,false,true);
                        loopRequired= true;
                    }

                    temp = checkPresence(replace);
                    insertNode(temp, false, second);
                    insertLink(first, temp);
                    insertLink(third, temp);

                    equation.replace(pos_start, pos_end - pos_start + 1, temp);
                    getData.clear();
                }


            }
            else{     // ELSE SENZA PARENTESI

                getData.str(equation);
                getData >> first >> second >> third;
                getData.clear();

                if (first == "NOT") {
                    for (int i = 0; i < _inputVect.size(); i++) {
                        if (second == _inputVect[i].name) {
                            second = checkPresence(second);       //Verificare la reference e il counter/index
                            insertNode(second, true);
                        }
                    }
                    for(int j=0 ;j< GeneralOutput.size();j++)
                    { if(second == GeneralOutput[j].name && second != name)
                        {
                            second = equationParser(GeneralOutput[j].equation,GeneralOutput[j].name,_inputVect,GeneralOutput,_flipflop);
                        }

                    }
                    for(int z=0; z<_flipflop.size();z++){
                        if( second == _flipflop[z].name && second != name){
                            fliptemp = equationParser(_flipflop[z].equation,_flipflop[z].name,_inputVect,GeneralOutput,_flipflop);
                            second=checkPresence(second);
                            insertNode(second,false,"FF"); //qui inserisco il flipflop
                            insertLink(fliptemp,second);
                        }

                    }
                    if(second == name) //sempre per i loop matteo
                    {
                        second= checkPresence(second);
                        insertNodeLoop(second,false,true);
                        loopRequired= true;
                    }

                    temp = checkPresence(replace);
                    insertNode(temp, false, first);
                    insertLink(second,temp);

                    equation.replace(0, equation.size(), temp);                       //Perché il +1??
                    getData.clear(); }

                else {
                    for (int i = 0; i < _inputVect.size(); i++) {                   //
                        if(first == _inputVect[i].name){
                            first = checkPresence(first);
                            insertNode(first, true);
                        }

                        if(third == _inputVect[i].name){
                            third = checkPresence(third);
                            insertNode(third, true);
                        }
                    }

                    for(int j=0 ;j< GeneralOutput.size();j++)
                    {
                        if(first == GeneralOutput[j].name && first != name)
                        {
                            first = equationParser(GeneralOutput[j].equation,GeneralOutput[j].name,_inputVect,GeneralOutput,_flipflop);
                        }
                        if(third== GeneralOutput[j].name && third != name)  // CONTROLLARE CHE EQUATION RITORNI L' ULTIMO NODO INSERITO
                        {
                            third = equationParser(GeneralOutput[j].equation,GeneralOutput[j].name,_inputVect,GeneralOutput,_flipflop);
                        }
                    }
                    for(int z=0; z<_flipflop.size();z++){
                        if( first == _flipflop[z].name && first != name){
                            fliptemp = equationParser(_flipflop[z].equation,_flipflop[z].name,_inputVect,GeneralOutput,_flipflop);
                            first=checkPresence(first);
                            insertNode(first,false,"FF"); //qui inserisco il flipflop
                            insertLink(fliptemp,first);
                        }
                        if( third == _flipflop[z].name && third != name){
                            fliptemp = equationParser(_flipflop[z].equation,_flipflop[z].name,_inputVect,GeneralOutput,_flipflop);
                            third= checkPresence(third);
                            insertNode(third,false,"FF"); //qui inserisco il flipflop
                            insertLink(fliptemp,third);
                        }
                    }
                    if(first == name){ // per i loop
                        first= checkPresence(first);
                        insertNodeLoop(first,false,true);
                        loopRequired= true;
                    }
                    if (third == name){
                        third= checkPresence(third);
                        insertNodeLoop(third,false,true);
                        loopRequired= true;
                    }
                    temp = checkPresence(replace);
                    insertNode(temp, false,second);                            //Inserisce il nodo sost.x con secondo = logicInfo
                    insertLink(first, temp);
                    insertLink(third, temp);

                    equation.replace(0, equation.size(), temp);
                    getData.clear();
                }
            }
        }
    }
    else{
        //cerr<< "Wrong format for \"assign\" execution." << endl;            //Errore sintattico
        return "error";
    }


    return equation;
}

int Graph::parenthesisCounter(string equation) {                        //Conta le parentesi aperte
    int counter = 0;

    /*for (int i = 0; i < equation.size(); i++) {
        if(equation[i]=='('){
            counter++;
        }
        else{
            if(equation[i]==')'){
                counter++;
            }
        }
    }*/

    counter = count(equation.begin(), equation.end(), '(');

    return counter;
}

int Graph::logicCounter(const string &equation) {                             //Testato, funziona. Conta le porte logiche nell'equazione
    int counter = 0;
    int pos = 0;
    vector<string> logicGate = {"AND", "OR", "NOT", "XOR" };
    vector<string>::iterator it = logicGate.begin();

    for (; it < logicGate.end(); it++) {
        do{
            pos=equation.find(*it, pos);
            if(pos!=string::npos){
                counter++;
                pos++;
            }
        }while(pos!=string::npos);
        pos=0;
    }

    return counter;
}

int Graph::powerCalculator(int temp, vector<power> dataPower) {
    int i = 0;

    while ((circuito[temp].logicInfo != dataPower[i].name) && i < dataPower.size()) {
        i++;
    }
    if ((i - 1) > dataPower.size()) {
        return -1; // power porta logica non trovato
    }

    if (circuito[temp].logicInfo == "NOT" || circuito[temp].logicInfo == "FF") {
        if (circuito[temp].inputValue1 != circuito[temp].outputValue) {
            if (circuito[temp].inputValue1 && !circuito[temp].outputValue) {
                totalpower = totalpower + dataPower[i].from1to0;
            } else {
                if (!circuito[temp].inputValue1 && circuito[temp].outputValue)
                    totalpower = totalpower + dataPower[i].from0to1;
            }
        }
    } else {
        if (circuito[temp].outputValue != circuito[temp].inputValue1) {
            if (circuito[temp].inputValue1 && !circuito[temp].outputValue) {
                totalpower = totalpower + dataPower[i].from1to0;
            } else {
                if (!circuito[temp].inputValue1 && circuito[temp].outputValue) {
                    totalpower = totalpower + dataPower[i].from0to1;
                }
            }
        }
        if (circuito[temp].outputValue != circuito[temp].inputValue2) {
            if (circuito[temp].inputValue2 && !circuito[temp].outputValue) {
                totalpower = totalpower + dataPower[i].from1to0;
            } else {
                if (!circuito[temp].inputValue2 && circuito[temp].outputValue) {
                    totalpower = totalpower + dataPower[i].from0to1;
                }
            }
        }
    }
}

string Graph::replaceComb(const string &equation1, string to_find, string to_sub){
    size_t pos=0, pos2=0;
    string equation = equation1;
    pos = equation.find(to_find);
    pos2 = to_find.size();
    equation.replace(pos, pos2, to_sub);
    return equation;
}

string Graph::dollarConverter(const string &to_check) {
    string copy;
    size_t pos;

    pos = to_check.find("$$");
    if(pos!=string::npos){
        for (int i = 0; i < pos; ++i) {
            copy += to_check[i];                            //Prende tutte le lettere prima di $$
        }
        return copy;
    }
    return to_check;
}

bool Graph::GetLoopRequired() const {
    return loopRequired;
}


/*vector<string> Graph::logicCones(const string &outputName) {

}*/
